Rebol [
    Author: "Graham Chiu"
    Date: 19-April-2019
    Notes: {
        uses the chess board and js from https://github.com/oakmac/chessboardjs

        Previous to the modularization requirements of Ren-C, using this with DO
        would write additional commands (like CHESS-HELP) into the user context.
        It also overwrote CLEAR (a common Rebol function).  It changed the
        console prompt to indicate the console had been augmented as `chess>>`

        In order to cooperate with modularization, this would have to be a
        module and EXPORT some functions that you'd get from `import @chess`.
        But since there is already some console overriding going on, this tries
        a different approach of overriding the console to bind code into this
        module.  This is an experiment which allows the `do @chess` syntax
        to work for the time being.

        That doesn't go well with scripting, since that's done all at once.  A
        temporary workaround is to return the animate-game function.
    }
]

=== CUSTOMIZATIONS THAT SHOULD BE IN A COMMON "LIBCHIU" LIBRARY ===

; Customize FUNC to not require a RETURN--result drops out of body by default
; https://forum.rebol.info/t/making-func-variant-that-auto-returns-last-result/2124
;
func: adapt :lib.func [
    body: inside body bindable compose [
        return (as group! bindable body)
    ]
]
meth: enfix adapt :lib.meth [
    body: inside body bindable compose [
        return (as group! bindable body)
    ]
]


=== LIBRARIES ===

import @popupdemo

js-do https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js

; Was: css-do https://raw.githubusercontent.com/oakmac/chessboardjs/master/src/chessboard.css

css-do https://raw.githubusercontent.com/oakmac/chessboardjs/ffcdebadb14cf4c9d123175a448c5bb5e52b3cab/src/chessboard.css

; Was: js-do https://raw.githubusercontent.com/oakmac/chessboardjs/master/src/chessboard.js

js-do https://raw.githubusercontent.com/oakmac/chessboardjs/ffcdebadb14cf4c9d123175a448c5bb5e52b3cab/src/chessboard.js


=== MAIN SCRIPT ===

player: false ; if true, white's turn
toggle: does [player: not player]

;replpad-write/html {<div id="board" style="width: 400px"></div>}
show-dialog/size {<div id="board" style="width: 400px"></div>} 480x480

clear: js-awaiter [
    {clear the board}
]{
    board.clear()
}

startboard: js-awaiter [
    {reinitialise the board}
]{
    board.start()
}

start: func [<with> player] [
    player: false
    startboard
]

set-board: js-awaiter [
    {setup board using fen}
    fen [text!]
]{
    board.position(reb.Spell(reb.ArgR('fen')))
}

Ruy-Lopez-position: "r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R"

ruy-lopez: func [
    {set position to Ruy Lopez}
][
    set-board ruy-lopez-position
]

promote: func [from to-move piece] [
    ; make the move first to the 8th rank
    move unspaced [from "-" to-move]
    position: js-eval {board.fen()}
    ; now we either promote black or white
    if to-move.2 = #"8" [
        ; promote white piece
        replace position "P" uppercase piece
    ] else [
        if to-move.2 = #"1" [
            ; promote black pawn
            reverse position
            replace position "p" lowercase piece
            reverse position
        ]
    ]
    ; now reset the board
    set-board position
]

move: js-awaiter [
    {use chess notation}
    chess-move [text!]
]{
    board.move(reb.Spell(reb.ArgR('chess-move')))
}

js-do {var cfg = {
  draggable: true,
  dropOffBoard: 'snapback', // this is the default
  moveSpeed: 'slow',
  position: 'start'
};}


js-do {var board = ChessBoard('board', cfg)}

peek-fen: func [
    {returns the piece at an.  Eg. "4Pp2" "6" => "p", and "4Pp2" "e" => "P"}
    row [text!]
    an [text! char?]
][
    if row = "8" [return ""] ; empty row
    let chess-cols: charset [#"a" - #"h" #"A" - #"H"]
    if ok? parse an [chess-cols] [
        ;print "a column found"
        an: index? find "abcdefgh" an
        ;-- an
    ] else [
        an: to integer! an
    ]
    let chess-rows: charset [#"1" - #"8"]
    let piece: charset [ #"p" #"P" #"r" #"n" #"b" #"q" #"Q" #"B" #"N" #"R" #"k" #"K" ]
    let i: 0
    let col
    parse row [
        some [
            [col: across piece (i: me + 1) | col: across chess-rows (i: me + to integer! col)]
            (
                ;print ["Col: " col "i: " i]
                if i >= an [
                    ;print "returning from peek-ren"
                    ;-- col
                    parse col [chess-rows] except [return col]
                    return copy ""
                ]
            )
        ]
    ]
    ; got to end of row and no pieces found
    return ""
]

comment {
    if the piece is a pawn that moves from 5th to 6th row at an angle, then remove an adjacent pawn on the 5th row
}

enpassant: func [
    {capture pawn en passant}
    from-move to-move
    <with> player
][
    if player [  ; print "white enpassant check"
      ; check if the move is from 5th to 6th - white doing an en passant
      if all [from-move.2 = #"5" | to-move.2 = #"6"][
          ; check to see if changing columns
          let candidateMoveL: unspaced [to-move.1 - 1 to-move.2 - 1]
          let candidateMoveR: unspaced [to-move.1 + 1 to-move.2 - 1]
          if find reduce [candidateMoveL candidateMoveR] from-move [
              ; print "from a right square"
              ; now check the to square to see if it's empty
              let position: split js-eval {board.fen()} "/"
              let row6: pick position 3
              ;-- row6
              ;probe  peek-fen row6 to-move.1
              ;-- to-move
              if empty? peek-fen row6 to-move.1 [
                  ;print "Moving to empty square"
                  ; empty square we are moving to so now check to see if there's a pawn to take
                  let row5: pick position 4
                  ;peek-fen row5 to-move.1
                  if "p" == peek-fen row5 to-move.1 [
                      ; now see if we are a pawn moving from
                      if "P" == peek-fen row5 from-move.1 [
                          ;print "valid en passant for White"
                          return true
                      ]
                  ]
              ]
          ]
      ]
    ] else [ ; print "black enpassant check"
      ; check if the move is from 5th to 6th - white doing an en passant
      if all [from-move.2 = #"4" | to-move.2 = #"3"][
          ; check to see if changing columns
          let candidateMoveL: unspaced [to-move.1 - 1 to-move.2 + 1]
          let candidateMoveR: unspaced [to-move.1 + 1 to-move.2 + 1]
          if find reduce [candidateMoveL candidateMoveR] from-move [
              ; print "from a right square"
              ; now check the to square to see if it's empty
              let position: split js-eval {board.fen()} "/"
              let row3: pick position 6
              if empty? peek-fen row3 to-move.1 [
                  ; print "Moving to empty square"
                  ; empty square we are moving to so now check to see if there's a pawn to take
                  let row4: pick position 5
                  peek-fen row4 to-move.1
                  if "P" == peek-fen row4 to-move.1 [
                      ; now see if we are a pawn moving from
                      if "p" == peek-fen row4 from-move.1 [
                          ; print "valid en passant for Black"
                          return true
                      ]
                  ]
              ]
          ]
      ]
    ]
    return false
]

replpad-write/html {
   <input type="button" id="startBtn" value="Start" onclick="board.start()" />
   <input type="button" id="clearBtn" value="Clear" onclick="board.clear()" />
}

detect-chess-move: func [
    {Detect chess moves in the console}
    return: "True if it was a move notation (and this routine processed it)"
        [logic?]
    s [text!]
][
    let queensidecastle: null
    let kingsidecastle: null
    let piece: null
    let chess-cols: charset [#"a" - #"h" #"A" - #"H"]
    let chess-rows: charset [#"1" - #"8"]
    let valid-square: [chess-cols chess-rows]
    let promotion: charset [ #"r" #"n" #"b" #"q" #"Q" #"B" #"N" #"R" ]
    let from-move
    let to-move
    let valid-move: [
        try some space
        any [
            [
                from-move: across valid-square
                some space
                to-move: across valid-square
                try ["=" piece: across promotion]
            ]
            [queensidecastle: across "O-O-O"]
            [kingsidecastle: across "O-O"]
        ]
    ]
    return ok? parse s [ some [
        valid-move (
            ; player: not player
            toggle
            ; print if player ["white"] else ["black"]
            case [
                piece [
                    ; we got promoted
                    promote from-move to-move piece
                ]

                queensidecastle [
                    ;print "Queen castling"
                    if player [move "e1-c1" move "a1-d1"] else [move "e8-c8" move "a8-d8"]
                ]

                kingsidecastle [
                    ;print "King castling"
                    if player [move "e1-g1" move "h1-f1"] else [move "e8-g8" move "h8-f8"]
                ]

                true [
                    if enpassant from-move to-move [
                        ; move to the adjacent square to remove the pawn and then forward
                        move unspaced [from-move "-" to-move.1 from-move.2 ]
                        move unspaced [to-move.1 from-move.2 "-" to-move ]
                    ] else [
                        move unspaced [from-move "-" to-move]
                    ]
                ]
            ]
        )
        some space
    ]]
]

animate-game: func [
    moves [block!]
][
    clear start
    for-each mv moves [
        mv: form mv
        if mv = "e8g8" [
            move "e8-g8"
            move "h8-f8"
        ] else [
            insert skip mv 2 "-"
            move mv          
        ]
        wait 1
    ]
    return <done>
]

; do https://gitlab.com/Zhaoshirong/rebol-chess/raw/master/chiu-jensen.reb


chiu-vs-jensen: func [
    {plays one of my old games}
][
    print "Graham Chiu vs Kai Jensen, 83rd NZ Ch Premier Reserve (1976), Upper Hutt NZL"
    animate-game [
        e2e4 e7e5 
        g1f3 b8c6
        b1c3 g8f6
        d2d4 e5d4
        c3d5 f6e4
        d1e2 f7f5
        f3g5 f8e7
        g5e4 f5e4
        e2e4 e8g8
        f1d3 e7b4
        e1d1 g7g6
        c1h6 f8f5
        d3c4 g8h8
        d5b4 c6b4
        h1e1 d7d5
        e4e8 f5f8
        e8e5
    ]
    print "Black Resigns.  Thanks Kai!"
]

system.console.prompt: "chess>>"

; console code https://github.com/metaeducation/ren-c/blob/master/extensions/console/ext-console-init.reb
system.console: make system.console [
    input-hook: meth [] [
        s: read-line
        append s " "
        return if detect-chess-move s [""] else [s]
    ]
    dialect-hook: meth [b [block!]] [
        ; mutably bind code into this module for starters
        inside system.contexts.user bind b (binding of inside [] 'startboard)
    ]
]

chess-help: does [
    print delimit newline [
        {Note, this is a demo and not a real application, yet}
        {clear to clear the board}
        {start to set up a new board}
        {Algebraic notation to move pieces: e2 e4}
        {Use letters for castling eg. o-o and o-o-o}
        {Set-board fen eg. for a Ruy Lopez position set-board "r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R"}
        {For castling, en passant to work, type 'player: false' for white to move, and 'player: true' for black to move after setting up a position}
        {For pawn promotion: e7 e8=N to promote pawn to Knight}
        {For en passant, just move the pawn as normal eg. e5 d6}
        {Sample game type: chiu-vs-jensen}
    ]
    replpad-write/html unspaced [{<a href="} https://gitlab.com/Zhaoshirong/rebol-chess/issues {">And for bug reports</a>}]
]

; set-board "rnbqkbnr/ppp2ppp/4p3/3pP3/8/8/PPPP1PPP/RNBQKBNR" ;"rnbqkbnr/1ppp1ppp/8/2PPpP2/pP6/4P3/P5PP/RNBQKBNR"
; rows: split js-eval {board.fen()} "/"
; row: pick rows 4

print "Type chess-help for instructions"

; Return the ANIMATE-GAME function so that automation can test it.
:animate-game
