Rebol [
    notes: {game played long ago when I was 16}
]

chiu-vs-jensen: function [{plays one of my old games}][
    print "Graham Chiu vs Kai Jensen, 83rd NZ Ch Premier Reserve (1976), Upper Hutt NZL"
    clear start
    for-each mv [
        e2e4 e7e5 
        g1f3 b8c6
        b1c3 g8f6
        d2d4 e5d4
        c3d5 f6e4
        d1e2 f7f5
        f3g5 f8e7
        g5e4 f5e4
        e2e4 e8g8
        f1d3 e7b4
        e1d1 g7g6
        c1h6 f8f5
        d3c4 g8h8
        d5b4 c6b4
        h1e1 d7d5
        e4e8 f5f8
        e8e5
    ][
        mv: form mv
        if mv = "e8g8" [
            move "e8-g8"
            move "h8-f8"
        ] else [
            insert skip mv 2 "-"
            move mv            
        ]
        wait 1
    ]
    print "Black Resigns.  Thanks Kai!"
]
